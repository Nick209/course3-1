package ru.omsu.imit;

import com.sun.xml.internal.messaging.saaj.soap.impl.TreeException;

import java.util.Arrays;

public class Group {
    private String gname;
    private Trainee[] trainees;

    public Group(String gname, Trainee[] trainees) throws TraineeException {
        if (gname == null || gname == "" || trainees == null || trainees.length == 0) throw new TraineeException();
        this.gname = gname;
        this.trainees = trainees;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) throws TraineeException {
        if (gname == null || gname == "") throw new TraineeException();
        gname = gname;
    }

    public Trainee[] getTrainees() {
        return trainees;
    }

    public void setTrainees(Trainee[] trainees) throws TraineeException {
        if (trainees == null || trainees.length == 0) throw new TraineeException();
        this.trainees = trainees;
    }

    @Override
    public String toString() {
        return "Group{" +
                "trainees=" + Arrays.toString(trainees) +
                '}';
    }

}
