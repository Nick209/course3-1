package ru.omsu.imit;

public class Institute {
    private String iname;
    private String city;

    public Institute(String iname, String city) throws TraineeException {
        if (iname == null || iname == "" || city == null || city == ""  ) throw new TraineeException();
       this.iname = iname;
       this. city = city;
    }

    public String getIname() {
        return iname;
    }

    public void setIname(String iname) {
        this.iname = iname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
