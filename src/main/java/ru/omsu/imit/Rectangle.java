package ru.omsu.imit;

import java.io.*;
import java.util.Objects;
import java.lang.Object;

public class Rectangle {
    private int left, right, bottom, top;

    public Rectangle(int left, int right, int top, int bottom) {
        this.bottom = bottom;
        this.left = left;
        this.right = right;
        this.top = top;
    }

    public Rectangle() {
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle)) return false;
        Rectangle rectangle = (Rectangle) o;
        return getLeft() == rectangle.getLeft() &&
                getRight() == rectangle.getRight() &&
                getBottom() == rectangle.getBottom() &&
                getTop() == rectangle.getTop();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getLeft(), getRight(), getBottom(), getTop());
    }

    public static Rectangle outObj(Rectangle r) { // 9
        Rectangle z = new Rectangle(0, 0, 0, 0);

        try (FileInputStream fis = new FileInputStream(new File("C:\\Проекты студентов и преподавателей\\3 курс\\Obukhov N\\course3-1\\doc.txt"))) {
            try (DataInputStream dis = new DataInputStream(fis)) {
                z.setBottom(dis.readInt());
                z.setLeft(dis.readInt());
                z.setRight(dis.readInt());
                z.setTop(dis.readInt());
                System.out.println(r);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return z;
    }

    public static Rectangle[] fiveOutRectangles(Rectangle[] r) {
        try (FileInputStream fis = new FileInputStream(new File("C:\\Проекты студентов и преподавателей\\3 курс\\Obukhov N\\course3-1\\doc.txt"))) {
            try (DataInputStream dis = new DataInputStream(fis)) {
                PrintStream printStream = new PrintStream(new File("C:\\Проекты студентов и преподавателей\\3 курс\\Obukhov N\\course3-1\\doc.txt"));
                for (int i = 0; i < r.length;i++ ) {
                    r[i].setLeft(dis.readInt());
                    r[i].setTop(dis.readInt());
                    r[i].setRight(dis.readInt());
                    r[i].setBottom(dis.readInt());
                    printStream.println();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return r;

    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Rectangle{");
        sb.append("left=").append(left);
        sb.append(", right=").append(right);
        sb.append(", bottom=").append(bottom);
        sb.append(", top=").append(top);
        sb.append('}');
        return sb.toString();
    }
}






