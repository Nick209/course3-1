package ru.omsu.imit;

import java.io.Serializable;
import java.time.temporal.Temporal;
import java.util.Comparator;
import java.util.Objects;

public class Trainee implements Serializable, Comparator<Trainee>,Comparable<Trainee> {

    public static final String ERROR_CODE = "ERROR";
    public static final String ILLEGAL_ARGUMENT = "ILLEGAL ARGUMENT";

    private String name;
    private String surname;

    @Override
    public String toString() {
        return name + " " + surname + " " + N;
    }

    private int N;
    private numbers n;

    public void setN(int n) {
        N = n;
    }

    public int getN() {
        return N;
    }

    public void setN(numbers n) {
        this.n = n;
    }

    @Override
    public int compareTo(Trainee t) {
       return name.compareTo(t.getName());
    }

    public enum numbers {
        ONE(1),
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5);
        private numbers(){}
        private int value;
        private numbers(int value){
            this.value = value;
        }
    };

    enum TraineeErrorCodes {
        ERROR_CODE,
        ILLEGAL_ARGUMENT
    };

    Trainee() { if (name.isEmpty() || surname.isEmpty()) Trainee.getErrorCode(); }

    Trainee(String name, String surname, numbers n) {
        if (name == null ||name.isEmpty() || surname == null ||  surname.isEmpty()|| n == null ) Trainee.getErrorCode(); //везде так сделать
        this.name = name;
        this.surname = surname;
        this.n = n;
    }
    Trainee (String name, String surname, int N ) throws TraineeException {
        if ( name == null || surname == null || N < 2 || N > 5  ) throw new TraineeException();
        this.name = name;
        this.surname = surname;
        this.N = N;
    }

    public static String getErrorCode() {
        return ERROR_CODE;
    }

    public static String getIllegalArgument() {
        return ILLEGAL_ARGUMENT;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setName(String name) {
        if (name.isEmpty()) Trainee.getErrorCode();
        this.name = name;
    }

    public void setSurname(String surname) {
        if (surname.isEmpty()) Trainee.getErrorCode();
        this.surname = surname;
    }

    @Override
    public int compare(Trainee o1, Trainee o2) {

        return o1.getN();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trainee trainee = (Trainee) o;
        return Objects.equals(name, trainee.name) &&
                Objects.equals(surname, trainee.surname);
    }

    public String str(){
        return name + " \r\n " +
                surname + "\r\n " +
                N ;
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, surname);
    }


}
