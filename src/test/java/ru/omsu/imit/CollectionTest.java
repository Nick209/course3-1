package ru.omsu.imit;

import org.junit.Test;

import javax.swing.table.TableCellRenderer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class CollectionTest {
    @Test
    public void createTrainee() throws TraineeException {
        Trainee tr = new Trainee(null,null,0);
        Group gr = new Group(null,null);
    }
    @Test
    public void sortGroup() throws TraineeException {
        Trainee[] tr = new Trainee[5];
        for (int i = 0; i < tr.length; i++) {
            tr[i] = new Trainee("gfdgdf", "gdfgddf", (int) (10 * Math.random() / 5) + 3);
        }
        Group g = new Group("mpb-603",tr);
        for (int i = tr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (tr[j].getN() > tr[j + 1].getN()) {
                    int tmp = tr[j].getN();
                    tr[j] = tr[j + 1];
                    tr[j + 1].setN(tmp);
                }
            }
            System.out.println(tr[i].getN());
        }
    }
    @Test
    public void sortName() throws TraineeException {
        Trainee [] tr = new Trainee[5];
        String[] names = new String[]{"nikita","tanya","dasha","victor","andrey"};
        for (int i = 0; i < tr.length; i++){
            tr[i] = new Trainee(names[i],"any",5);
            System.out.println(tr[i].getName());
        }
        Arrays.sort(tr);
        Group g = new Group("MMS",tr );
        System.out.println(g.toString());
    }
    @Test
    public void findName() throws TraineeException{
        Trainee [] tr = new Trainee[5];
        String[] names = new String[]{"nikita","tanya","dasha","victor","andrey"};
        for (int i = 0; i < tr.length; i++){
            tr[i] = new Trainee(names[i],"any",5);
            System.out.println(tr[i].getName());
        }
        String s = "nikita";
        for (int i = 0 ; i < tr.length; i++){
            if (s.equals(tr[i].getName())){
                System.out.println(tr[i]);
            }
        }
    }
    @Test
    public void arratListTraineeTest() throws TraineeException {
        ArrayList<Trainee> tr = new ArrayList<>();
        ArrayList<Trainee> tr12 = new ArrayList<>();
        Trainee[] tr1 = new Trainee[5];
        String[] names = new String[]{"nikita", "tanya", "dasha", "victor", "andrey"};
        Random r = new Random();

        for (int i = 0; i < tr1.length; i++) {
            int rand = r.nextInt(4)+2;
            tr1[i] = new Trainee(names[i], "any", rand);
            tr.add(i, tr1[i]);
        }

        System.out.println("Коллекция заполнена:" + tr);
        Collections.reverse(tr);
        System.out.println("Колекция перевернута:" + tr); //9.1
        Collections.rotate(tr,2);
        System.out.println("Колекция сдвинута на 2 эл-та:" + tr); // 9.2
        Collections.shuffle(tr);
        System.out.println("Коллекция перемешана:" + tr); //9.3
        Collections.sort(tr);

        System.out.println(tr);

    }
}
