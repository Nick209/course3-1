package ru.omsu.imit;

import org.junit.Test;
import ru.omsu.imit.Rectangle;

import javax.swing.filechooser.FileFilter;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class FileTest {
public static final String path = " C:\\Проекты студентов и преподавателей\\3 курс\\Obukhov N\\course3-1\\doc.txt";
    @Test
    public void Test(){
        try {
            String content = "This is the content to write into create file";

            File file = new File(path);
            if (!file.exists()) file.createNewFile();

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(content);
            bw.close();
            file.delete();

        } catch (Exception e) {
            System.out.println(e);
        }


    }


    @Test
    public void createFileTest() {
        File file = new File(path);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File f = new File("C:\\Проекты студентов и преподавателей\\3 курс\\Новая папка\\course3-1/doc.txt");
        System.out.println(f.getAbsolutePath());
        assertEquals(true, f.exists());
    }

    @Test
    public void deleteFileTest() {
        try {
            String content = "This is the content to write into create file";

            File file = new File(path);
            if (!file.exists()) file.createNewFile();

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write(content);
            bw.close();
            assertEquals(true, file.delete());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void renameFileTest() throws IOException {
        String content = "This is the content to write into create file";

        File newDir = new File("C:\\Проекты студентов и преподавателей\\3 курс\\Новая папка\\course3-1/doc.txt");
        File file = new File(path);
        if (!file.exists()) file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write(content);
        bw.close();
        assertEquals(true, file.renameTo(newDir));


    }

    @Test
    public void getFullNameTEst() throws IOException {
        String content = "This is the content to write into create file";

        File newDir = new File("C:\\Проекты студентов и преподавателей\\3 курс\\Новая папка");
        File file = new File(path);
        if (!file.exists()) file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write(content);
        bw.close();
        assertEquals(file.getAbsolutePath(), path);

    }

    @Test
    public void testIsFileTest() throws IOException {
        String content = "This is the content to write into create file";

        File newDir = new File("C:\\Проекты студентов и преподавателей\\3 курс\\Trainee\\doc.txt");
        File file = new File(path);
        if (!file.exists()) file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write(content);
        bw.close();
        assertEquals(true, file.isFile() && file.exists());

    }

    @Test
    public void getAllFileTest() throws IOException {
        String content = "This is the content to write into create file";

        File file = new File(path);
        if (!file.exists()) file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write(content);
        bw.close();
        System.out.println(Arrays.toString(file.list()));
    }


}