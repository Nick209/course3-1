package ru.omsu.imit;

import com.sun.xml.internal.ws.encoding.soap.SerializationException;
import org.junit.Test;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NIOmethodsTest implements Serializable {

    public static final String path = "C:\\Users\\Student\\900654876277a0ebe69b9848f2a76cf4d7beae7a\\doc.txt";

    @Test
    public void byteBufTest() throws TraineeException, Exception { // 1
        try (FileChannel channel = new FileInputStream(new File("doc.txt")).getChannel()) {
            ByteBuffer buffer = ByteBuffer.allocate((int) new File("doc.txt").length());
            channel.read(buffer);
            byte[] b = buffer.array();
            String s = new String(b, "UTF8");
            System.out.println(s);
            String[] s1 = s.split(" ");
            for (int i = 0; i < s1.length; i++) {
                System.out.println(s1[i]);
            }
            int n1 = Integer.parseInt(s1[2]);
            System.out.println(n1);

            Trainee t = new Trainee(s1[0], s1[1], n1);
            System.out.println(t.toString());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void mappedByteBuffer() throws TraineeException, Exception { // 2
        try (FileChannel channel = new RandomAccessFile(new File("doc.txt"), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, new File("doc.txt").length());
            channel.read(buffer);
            byte[] b = new byte[(int) new File("doc.txt").length()];

            for (int i = 0; i < b.length; i++) {
                b[i] = buffer.get(i);
            }
            String s1 = new String(b, "UTF8");
            System.out.println(s1);
            String s2[] = s1.split(" ");

            Trainee t = new Trainee(s2[0], s2[1], Integer.parseInt(s2[2]));
            System.out.println(t);


        }

    }

    @Test
    public void mapBufNumbers() throws IOException {
        try (FileChannel channel = new RandomAccessFile(new File("doccer.txt"), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, 100);
            byte[] b = new byte[100];
            for (int i = 0; i < 100; i++) {
                b[i] = (byte) i;
            }
            buffer.put(b);
            //buffer.force();
            // channel.write(ByteBuffer.wrap(b));


        }
        try (FileInputStream fis = new FileInputStream(new File("doccer.txt"));
             DataInputStream opsw = new DataInputStream(fis)) {
            byte[] buffer = new byte[fis.available()];
            opsw.read(buffer);
            //String s1 = new String(buffer,"UTF8");
            //System.out.println(s1);
        }
    }

    @Test
    public void serializeTraineeByte() throws TraineeException, IOException,SerializationException { //4
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            Trainee tr = new Trainee("Nikita", "Obukhov", 5);
            oos.writeObject(tr);
            ByteBuffer.wrap(baos.toByteArray());
        }
    }
    @Test
    public void pathFilesTest(){ // 5
        Path p = Paths.get("C:\\Users\\Student\\900654876277a0ebe69b9848f2a76cf4d7beae7a\\doc.txt");
        Path p1 = Paths.get("C:\\Users\\Student\\visitor");
        System.out.println(p.getRoot());
        System.out.println(p.getName(1));
        System.out.println(p.getFileName());
        System.out.println(p.getFileSystem());
        System.out.println(p.hashCode());
        System.out.println(p.getNameCount());
        System.out.println(p.isAbsolute());
        System.out.println(p.getParent());
        System.out.println(p.toUri());
        System.out.println(p.toFile());
        System.out.println(p.normalize());
        System.out.println(p.relativize(p1));
        System.out.println(p.resolve(p1));
        System.out.println(p.startsWith(p1));
        System.out.println(p);
    }
    @Test
    public void  renameFile() throws TraineeException{ //6
        File filesPath = new File("C:\\Users\\Student\\900654876277a0ebe69b9848f2a76cf4d7beae7a\\files");
        File[] filesList = filesPath.listFiles();

        for (int i = 0; i < filesList.length; i++) {

            String buf = filesList[i].getAbsolutePath();
            if (buf.endsWith(".dat")) {
                String[] s1 =  buf.split("\\.");
                System.out.println(buf);
                System.out.println(s1.length);

             //   rashir = rashir.replace(s1[s1.length-1],"bin");
                s1[s1.length-1] = "bin";
               String s = String.join(".", s1);
              //  buf = buf.replace(".dat", ".bin");

                filesList[i].renameTo(new File(s));
                System.out.println(filesList[i].toString());
                System.out.println(s);
                System.out.println("===============================");
            }
        }
    }
}


















