package ru.omsu.imit;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.junit.Test;
import org.junit.Assert;
import java.io.*;
import java.util.Scanner;

public class RectangleTest {
    public static final String path = "C:\\Users\\Student\\900654876277a0ebe69b9848f2a76cf4d7beae7a\\doc.txt";

    @Test
    public void createRectangleTest() { // 8
        Rectangle r = new Rectangle(129, 11, 12, 12);
        Rectangle q = new Rectangle(55, 45, 48, 44);
        try (FileOutputStream fos = new FileOutputStream("C:\\Проекты студентов и преподавателей\\3 курс\\Obukhov N\\course3-1\\doc.txt")) { //Запись интов в файл
            DataOutputStream dos = new DataOutputStream(fos);
            dos.writeInt(r.getBottom());
            dos.writeInt(r.getLeft());
            dos.writeInt(r.getRight());
            dos.writeInt(r.getTop());
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(r.equals(Rectangle.outObj(q)));
    }

    @Test
    public void outPutFiveRectangleTest() { // 10
        Rectangle[] rectangles = new Rectangle[5];
        for (int i = 0; i < rectangles.length; i++) {
            rectangles[i] = new Rectangle((int) (10 * Math.random()),
                    (int) (10 * Math.random()),
                    (int) (10 * Math.random()),
                    (int) (10 * Math.random()));
        }
        try (FileOutputStream fos = new FileOutputStream("C:\\Проекты студентов и преподавателей\\3 курс\\Obukhov N\\course3-1\\doc.txt")) { //Запись интов в файл{
            try (DataOutputStream dos = new DataOutputStream(fos)) {
                for (int i = 0; i < rectangles.length; i++) {
                    dos.writeInt(rectangles[i].getBottom());
                    dos.writeInt(rectangles[i].getLeft());
                    dos.writeInt(rectangles[i].getRight());
                    dos.writeInt(rectangles[i].getTop());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        try (FileInputStream fis = new FileInputStream("C:\\Проекты студентов и преподавателей\\3 курс\\Obukhov N\\course3-1\\doc.txt")) {
            DataInputStream dis = new DataInputStream(fis);
            for (int i = 0; i < rectangles.length; i++) {
                rectangles[i].setTop(dis.readInt());
                rectangles[i].setRight(dis.readInt());
                rectangles[i].setLeft(dis.readInt());
                rectangles[i].setBottom(dis.readInt());
            }
        } catch (Exception e) {
        } finally {
            for (int i = 0; i < rectangles.length; i++) {
                System.out.println(rectangles[0].toString());
            }
        }

    }

    @Test
    public void outPutStrTest() throws TraineeException { // 11
        Trainee tr = new Trainee("Nikita", "Obukhov", 5);
        System.out.println(tr.str());
        try {
            FileOutputStream fos = new FileOutputStream(path);
            DataOutputStream dos = new DataOutputStream(fos);
            dos.writeUTF(tr.str());
        } catch (Exception e) {


        }
    }

    @Test
    public void inPutStrTest() throws TraineeException { // 12
        try (FileInputStream fis = new FileInputStream(path)) {
            DataInputStream dis = new DataInputStream(fis);
            Trainee tr = new Trainee(dis.readUTF(), dis.readUTF(), dis.readInt());
            System.out.println(tr.str());

        } catch (Exception e) {

        }

    }

    @Test
    public void outPutStrTest13() throws TraineeException { // 13
        Trainee tr = new Trainee("Nikita", "Obukhov", 5);
        System.out.println(tr.toString());
        try(FileOutputStream fos = new FileOutputStream(path);
            OutputStreamWriter opsw = new OutputStreamWriter(fos,"UTF8")){
            opsw.write(tr.toString() +" ");
        } catch (Exception e) {
        }
    }

    @Test
    public void inPutStrTest14() throws TraineeException, IOException { // 14
        try(FileInputStream fis = new FileInputStream(path);
            DataInputStream dis = new DataInputStream(fis)) {

            Scanner scn = new Scanner(fis);
            String name, surname;
            name = scn.next();
            surname = scn.next();
            String str = scn.next();

            Assert.assertEquals( "5",str);
            int n =  Integer.parseInt(str.substring(0));
            Trainee tr = new Trainee(name, surname, n);
            System.out.println(tr.toString());

        }
    }

    @Test
    public void outPutObjTest() throws TraineeException {  // 15
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
Trainee tr = new Trainee(" NIKITA", " OBUKHOV", 5);
oos.writeObject(tr);

        } catch (Exception e) {

        }

    }
    @Test
    public void inPutObjTest() throws  TraineeException{ // 15
        try(FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ois = new ObjectInputStream(fis)) {
            Trainee tr = (Trainee)ois.readObject();
            System.out.println(tr.str());




        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void serializebleByteArrTest() throws TraineeException{ // 16
        Trainee tr = new Trainee(" fdgfd", " gfgdf",5);
        byte dst[] = new byte[tr.toString().length()*4];
        try(ByteArrayOutputStream b = new ByteArrayOutputStream();
            ObjectOutputStream o = new ObjectOutputStream(b) ){
            o.writeObject(tr);
            b.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }
        try( ByteArrayInputStream bais= new ByteArrayInputStream(dst);
             ObjectInputStream  ois = new ObjectInputStream(bais) ){
            ois.readObject();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
    @Test
    public void serializebleJSONTest() throws TraineeException, IOException { // 18,19
        Gson gson  = new Gson();
        Trainee tr = new Trainee(" gffdgd"," gfdgfd",4);
        String json = gson.toJson(tr);
        gson.toJson(tr,new FileWriter(path));
        JsonElement json1 = gson.fromJson(new FileReader(path),JsonElement.class);
        String res = gson.toJson(json);






    }




}


